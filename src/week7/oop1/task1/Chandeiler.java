package week7.oop1.task1;

/*
Включать и выключать люстру и показывать ее состояние
В люстре есть много ламп
*/
public class Chandeiler {
    private Bulb[] chandrlier;

    // конструктор люстры с созданием лампочек
    public Chandeiler(int countOfBulbs){
        chandrlier = new Bulb[countOfBulbs];

        for (int i = 0; i < countOfBulbs; i++) {
            chandrlier[i] = new Bulb();
        }
    }

    public void turnOn(){
        for (Bulb bulb: chandrlier)
            bulb.turnOn();
    }

    public void turnOf(){
        for (Bulb bulb: chandrlier)
            bulb.turnOf();
    }

    public boolean isShining(){
        return chandrlier[0].isShining();
    }
}
