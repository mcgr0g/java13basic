package week7.oop1.task1;

public class Main {
    public static void main(String[] args) {
        Bulb lamp = new Bulb();

        System.out.println("Светит ли лампа сейчас: " + lamp.isShining());
        lamp.turnOn();
        System.out.println("Светит ли лампа сейчас: " + lamp.isShining());

        Chandeiler chandeiler = new Chandeiler(4);
        chandeiler.turnOn();
        chandeiler.turnOf();


    }
}
