package week7.oop1.task1;
/*
Реализовать класс “Лампа”. Методы:
включить лампу
выключить лампу
получить текущее состояние
 */

public class Bulb {
    private boolean toogle = false;

    public Bulb(){
        this.toogle = false;
    }

    public Bulb(boolean condition){
        this.toogle = false;
    }

    void turnOn(){
        this.toogle = true;
    }

    void turnOf(){
        this.toogle = false;
    }

    boolean isShining(){
        return this.toogle;
    }
}
