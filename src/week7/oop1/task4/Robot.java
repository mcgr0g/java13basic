package week7.oop1.task4;
/*
Робот. Команды повернуть влево, повернуть вправо, идти.
Несколько конструкторов, хранение координат, вывод потом координат на экран.
 */

public class Robot {
    private int x;
    private int y;
    private int direction; //0 -- up, 1 -- right, 2 -- bottom, 3 -- left

    public Robot(){
        this.x = 0;
        this.y = 0;
        this.direction = 0;
    }

    public Robot(int x, int y){
        this.x = x;
        this.y = y;
    }

    public Robot(int x, int y, int direction ){
        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    public void go(){
        System.out.println("initial x: " + x);
        System.out.println("initial y: " + y);
        switch (direction){

            case 0: // to top
                y--;
                break;
            case 1: // to right
                x++;
                break;
            case 2: // to bottom
                y++;
                break;
            case 3: // to left
                x--;
                break;
        }
        System.out.println("new x: " + x);
        System.out.println("new y: " + y);
    }

    //0 -- up, 1 -- right, 2 -- bottom, 3 -- left
    public void ternLeft(){
        System.out.println("initial direction: " + direction);
        this.direction = (direction -1) % 4;
        System.out.println("new direction: " + direction);
    }

    public void ternRight(){
        System.out.println("initial direction: " + direction);
        this.direction = (direction +1) % 4;
        System.out.println("new direction: " + direction);
    }

    public void printCoordinates(){
        System.out.println("(x,y) = " + x + " " + y);
    }
}
