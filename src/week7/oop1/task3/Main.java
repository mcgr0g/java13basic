package week7.oop1.task3;

public class Main {
    public static void main(String[] args) {
        System.out.println(FieldValidator.validateEmail("andy@bk.ru"));
        System.out.println(FieldValidator.validateDate("10.40.2022"));
        System.out.println(FieldValidator.validateName("Vova"));
        System.out.println(FieldValidator.validatePhone("+799999999"));
    }
}
