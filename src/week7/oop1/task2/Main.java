package week7.oop1.task2;

public class Main {
    public static void main(String[] args) {
        Termometer termometer = new Termometer(-25, TemperatureUnit.CELSIUS);
        System.out.println("В цельсиях " + termometer.getTempCelsius());
        System.out.println("В фаренгейтах " + termometer.getTempFahrenheit());
    }
}
