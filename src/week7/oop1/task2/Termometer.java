package week7.oop1.task2;
/*

Реализовать класс “Термометр”.
Необходимо иметь возможность создавать инстанс класса с текущей температурой и
получать значение в фаренгейте и в цельсии.
 */

public class Termometer {
    private double tempCelsius;
    private double tempFahrenheit;

//    public Termometer(double curentTemperature, String temperatureUnit) {
//        if (temperatureUnit.equals("C")){
//            tempCelsius = curentTemperature;
//            tempFahrenheit = fromCelsiusToFahrenheit(curentTemperature);
//        }
//        else if (temperatureUnit.equals("F")){
//            tempCelsius = fromFahrenheitToCelsius(curentTemperature);
//            tempFahrenheit = curentTemperature;
//        }
//        else {
//            System.out.println("Температура не распознана");
////            throw new UnsupportedOperationException("Температура не распознана");
//        }
//
//    }

    public Termometer(double curentTemperature, TemperatureUnit temperatureUnit) {
        if (temperatureUnit == TemperatureUnit.CELSIUS) {
            tempCelsius = curentTemperature;
            tempFahrenheit = fromCelsiusToFahrenheit(curentTemperature);
        } else if (temperatureUnit == TemperatureUnit.FAHRENHEIT) {
            tempCelsius = fromFahrenheitToCelsius(curentTemperature);
            tempFahrenheit = curentTemperature;
        } else {
            System.out.println("Температура не распознана");
//            throw new UnsupportedOperationException("Температура не распознана");
        }

    }

    public double getTempCelsius() {
        return tempCelsius;
    }

    public double getTempFahrenheit() {
        return tempFahrenheit;
    }

    private double fromCelsiusToFahrenheit(double currentTemperature) {
        return currentTemperature * 1.8 + 32;
    }

    private double fromFahrenheitToCelsius(double currentTemerature) {
        return (currentTemerature - 32) / 1.8;
    }
}
