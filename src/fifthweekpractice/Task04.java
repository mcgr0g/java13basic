package fifthweekpractice;
/*
   На вход подается два отсортированных массива.
   Нужно создать отсортированный третий массив,
   состоящий из элементов первых двух.

   Входные данные:
   5
   1 2 3 4 7

   2
   1 6

   Выходные данные:
   1 1 2 3 4 6 7
    */

import java.util.Arrays;
import java.util.Scanner;

public class Task04 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr1 = new int[n];

        for (int i = 0; i < n; i++)
            arr1[i] = scanner.nextInt();

        int k = scanner.nextInt();
        int[] arr2 = new int[k];

        for (int i = 0; i < n; i++)
            arr2[i] = scanner.nextInt();


        mergeTwoArraysWithSysTools(arr1, arr2);

    }

    public static void mergeTwoArraysWithSysTools(int[] arr1, int[] arr2) {
        int[] mergedArray = new int[arr1.length + arr2.length];

        System.arraycopy(arr1, 0, mergedArray, 0, arr1.length);
        System.arraycopy(arr2, 0, mergedArray, arr1.length + 1, arr1.length + arr2.length);

        Arrays.sort(mergedArray);
        System.out.println(Arrays.toString(mergedArray));
    }

    public static void mergeTwoArrays(int[] arr1, int[] arr2) {
        int[] mergedArray = new int[arr1.length + arr2.length];
        int i = 0, j = 0, k = 0;

        //обход двух массивов
        while (i < arr1.length && j < arr2.length) {
            if (arr1[i] < arr2[j]) {
                mergedArray[k++] = arr1[i++];
            } else {
                mergedArray[k++] = arr2[j++];
            }
        }
        //сохраняем оставшиеся элементы первого массива
        while (i < arr1.length) {
            mergedArray[k++] = arr1[i++];
        }

        //сохраняем оставшиеся элементы второго массива
        while (j < arr2.length) {
            mergedArray[k++] = arr2[j++];
        }
        System.out.println(Arrays.toString(mergedArray));
    }

}
