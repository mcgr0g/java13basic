package fifthweekpractice;

/*
  На вход подается число N - длина массива.
     Затем передается массив строк длины N.
     После этого - число M.

     Сохранить в другом массиве только те элементы, длина строки которых
     не превышает M.

     Входные данные:
     5
     Hello
     good
     to
     see
     you
     4

     Выходные данные:
     good to see you
 */

import java.util.Scanner;

public class Task07 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String[] arr = new String[n];

        for (int i = 0; i < n; i++) {
            arr[i] = scanner.next();
        }

        int m = scanner.nextInt();

        int newArrayLength = 0;
        for (String elem: arr) {
            if (elem.length() <= m)
                newArrayLength++;
        }

        String[] arr2 = new String[newArrayLength];
        int cnt = 0;
        for (String elem: arr){
            if (elem.length() <= m)
                arr[cnt++] = elem;
        }
    }
}
