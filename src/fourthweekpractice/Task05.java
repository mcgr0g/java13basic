package fourthweekpractice;
/*
Дано целое число n, n > 0. Вывести сумму всех цифр этого числа.
92180 -> 20 //9 + 2 + 1 + 8 + 0 == 20
 */

import org.omg.CORBA.WStringSeqHelper;

import java.util.Scanner;

public class Task05 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int sum = 0;
        while (n > 0) {
            sum += n % 10;
            System.out.println("SUM in WHILE LOOP: " + sum);

            n = n / 10;
            System.out.println("N in WHILE LOOP: " + n);
        }

        System.out.println("Итоговый результат: " + sum);


//        for (int i = 0; i < str.length(); i++){
//            System.out.println(str.charAt(i));
//        }
    }
}
