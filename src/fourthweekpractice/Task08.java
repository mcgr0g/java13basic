package fourthweekpractice;

import java.util.Scanner;

public class Task08 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s= scanner.nextLine();

        int result = 0;

        for (int i=0; i < s.length(); i++){
            if (s.charAt(i) != ' ')
                result++;
        }

        System.out.println("Result: " + result);
    }
}
