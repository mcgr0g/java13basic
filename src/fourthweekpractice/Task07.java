package fourthweekpractice;
/*
На вход подается число n и последовательность целых чисел длины n.
Вывести два максимальных числа в этой последовательности без использования массивов.
5
1 3 5 4 5 -> 5 5
 */

import java.util.Scanner;

public class Task07 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int firstNumber = scanner.nextInt();
        int secondNumber = scanner.nextInt();

        int firstMax = Math.max(firstNumber, secondNumber);
        int secondMax = Math.min(firstNumber, secondNumber);


        if (n <= 2){
            System.out.println(firstNumber);
            System.out.println(secondNumber);
        } else {
            int next = 0;
            for (int i=2; i < n; i++){
                next = scanner.nextInt();
                if (next > firstMax){// переделать нафиг
                    secondMax = firstMax;
                    firstMax = next;

                } else if (next > secondMax) {
                    secondMax = next;
                }
            }
            System.out.println(firstMax + " " + secondMax);
        }
    }
}
