package fourthweekpractice;

import java.util.Scanner;

/* Дана последовательность из n целых чисел, которая может начинаться с
    отрицательного числа. Определить, какое количество отрицательных чисел
    записано в начале последовательности и прекратить выполнение программы
    при получении первого неотрицательного числа на вход.
Входные данные:
-1
-2
4
Выходные данные:
Result: 2
*/
public class Task09 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = 0;
        for (int i = scanner.nextInt(); i < 0; i = scanner.nextInt()) {
            count++;
        }
        System.out.println("result: " + count);

    }
}
