package CourceTasks;

public class RectangleUsage {
    public static void main(String[] args) {
        Rectangle r1 = new Rectangle(4, 40);

        Rectangle r2 = new Rectangle(3.5, 35.9);

        System.out.println("r1 about " + r1.width + " " + r1.height + " " + r1.getArea() + " " + r1.getPerimeter());
        System.out.println("r2 about " + r2.width + " " + r2.height + " " + r2.getArea() + " " + r2.getPerimeter());
    }
}
