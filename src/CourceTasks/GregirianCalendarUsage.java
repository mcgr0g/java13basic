package CourceTasks;

import java.util.GregorianCalendar;

public class GregirianCalendarUsage {
    public static void main(String[] args) {
        GregorianCalendar cal = new GregorianCalendar();

        System.out.println(cal.get(GregorianCalendar.YEAR));
        System.out.println(cal.get(GregorianCalendar.MONTH));
        System.out.println(cal.get(GregorianCalendar.DAY_OF_MONTH));

        cal.setTimeInMillis(1234567898765L);
        System.out.println(cal.get(GregorianCalendar.YEAR));
        System.out.println(cal.get(GregorianCalendar.MONTH));
        System.out.println(cal.get(GregorianCalendar.DAY_OF_MONTH));
    }
}
