package CourceTasks;

public class Stock {
    String symbol;
    String name;
    double previousClosingPrice;
    double currentPrice;

    Stock(String symbolToUse, String nameToUse, double previousClosingPriceToUse, double currentPriceToUse){
        symbol = symbolToUse;
        name = nameToUse;
        previousClosingPrice = previousClosingPriceToUse;
        currentPrice = currentPriceToUse;
    }

    public double getChangePercent(){
        return (currentPrice / previousClosingPrice);
    }
}
