package CourceTasks;

public class Rectangle {
    double width = -1;
    double height = -1;

    Rectangle(){
    }

    Rectangle(double customWidth, double customHeight){
        width = customWidth;
        height = customHeight;
    }

    public double getArea(){
        return width * height;
    }

    public double getPerimeter(){
        return 2 * (width + height);
    }
}
