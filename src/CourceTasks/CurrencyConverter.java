package CourceTasks;
import java.util.Scanner;

public class CurrencyConverter {
    static double ROUBLES_PER_DOLLAR = 72.12;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n; // количество конвертаций
        int[] dollarsArray; // массив сумм денег в американских долларах
        double[] roublesArray; // массив сумм денег в российских рублях
        instruct();

        do {
            System.out.print("Введите количество конвертаций: ");
            n = scanner.nextInt();
        }while (n<=0);

        dollarsArray = new int[n];
        System.out.println("Введите " + n + "сумм денег в "
        + "американских рублях через пробел");
        for (int i=0; i<n; i++) {
            dollarsArray[i] = scanner.nextInt(); // сумма денег в американских долларах
        }
        roublesArray = find_roubles(dollarsArray, n);

        System.out.println("\n   " + "Сумма, Р"+" " + "Сумма, $");
        for (int i = 0; i < n; i++)
            System.out.println("\t" + dollarsArray[i] + "\t\t" +
                    (int) (roublesArray[i]*100)/100.0);
    }
    /*
    * Отображает инструкцию
    * */
    public static void instruct(){
        System.out.println("Эта программа конвертирует сумму денег из американских "
                + "долларов в русские рубли");
        System.out.println("Курс покупки равен " + ROUBLES_PER_DOLLAR
                + "рубля.");
    }

    /*
    * Конвертирует из долларов в рубли каждый элемент
    * */
    public static double[] find_roubles(int[] dollarsArray, int currencyArrayLength){
        double[] roublesArray = new double[currencyArrayLength];
        for (int i = 0; i < currencyArrayLength; i++)
            roublesArray[i] = ROUBLES_PER_DOLLAR * dollarsArray[i];
        return roublesArray;
    }
}
