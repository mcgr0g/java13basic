package week8.oop2.task3;
/*
Примитивная реализация ArrayList.
Массив только int, из методов только добавлять элемент,
получать size и увеличивать капасити, когда добавляется новый.
 */

import java.util.Arrays;

public class SimleArrayList {
    private int size;
    private int[] arr;
    private int capacity;
    private static int DEFAULT_CAPACITY = 5;
    private int currIndex;

    public SimleArrayList() {
        arr = new int[DEFAULT_CAPACITY];
        capacity = DEFAULT_CAPACITY;
        size = 0;
        currIndex = 0;
    }

    public SimleArrayList(int size) {
        arr = new int[size];
        this.capacity = size;
        this.size = 0;
        currIndex = 0;
    }

    public void remove(int idx) {
        for (int i = idx; i < currIndex; i++) {
            arr[i] = arr[i+1];
        }
        size--;
        currIndex--;
    }

    public void add(int elem) {
        if (currIndex >= capacity) {
            capacity = 2 * capacity;
            arr = Arrays.copyOf(arr, capacity);
        }
        arr[currIndex] = elem;
        size++;
        currIndex++;
    }

    public int get(int idx) {
        if (idx < 0 || idx >= size) {
            System.out.println("Не возможно взять элемент по заданному индексу");
            return  -1;
        }
        else {
            return arr[idx];
        }
    }

    public int size(){
        return size;
    }
}
