package week8.oop2.task3;

public class Main {
    public static void main(String[] args) {
        SimleArrayList mySimpleArrayList = new SimleArrayList();
        mySimpleArrayList.add(12);
        mySimpleArrayList.add(13);
        System.out.println("Arraylist size: " + mySimpleArrayList.size());

        System.out.println("элемент с первым символом: " + mySimpleArrayList.get(0));
        System.out.println("элемент со второым символом: " + mySimpleArrayList.get(1));
        System.out.println("элемент с последним символом: " + mySimpleArrayList.get(mySimpleArrayList.size()-1));

    }
}
