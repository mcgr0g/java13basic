package week8.oop2.task1;

public class VariableLength {
    static int sum(int... numbers) {
        int sum = 0;

        for (int i = 0; i < numbers.length; i++)
            sum += numbers[i];

        return sum;
    }

    public static void main(String[] args) {

    }
}
