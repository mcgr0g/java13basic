package sixweekpractice;

import java.util.Scanner;

public class Task06 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[][] a = new int[n][n];

        for (int i = 0; i < n; i++) {
            for ( int j = 0; j < n; j++){
//                if (i % 2 == 0){
//                    if (j % 2 == 0){
//                        a[i][j] = 0;
//                    } else {
//                        a[i][j] = 1;
//                    }
//                } else if (j % 2 == 0){
//                    a[i][j] = 1;
//                } else {
//                    a[i][j] = 0;
//                }
                if ((i+j) % 2 == 0)
                    a[i][j] = 0;
                else
                    a[i][j] = 1;
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }
}
