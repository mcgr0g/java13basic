package sixweekpractice;

import java.util.Scanner;
/*
Развернуть строку рекурсивно.

abcde -> edcba
 */

public class Task03 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(reverseString(scanner.nextLine()));
    }

    public static String reverseString(String s){
        int l = s.length();
        if (l <= 1)
            return s;

        return s.substring(l-1,l)
                + reverseString(s.substring(1, l-1))
                + s.substring(0,1);
    }
}
