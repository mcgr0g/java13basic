package sixweekpractice;
/*
На вход подается натуральное число N.
Необходимо проверить, является ли оно степенью двойки (решить через рекурсию).
Вывести true, если является и false иначе.

4 -> true
5 -> false
6 -> false
7 -> false
8 -> true
 */

import java.util.Scanner;

public class Task02 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(check2pow(scanner.nextInt()));

    }

    public static boolean check2pow(int n){
        if (n == 2 || n == 1){
            return true;
        }
        if (n % 2 != 0 || n <= 0){
            return false;
        }

        return check2pow(n / 2);
    }
}
