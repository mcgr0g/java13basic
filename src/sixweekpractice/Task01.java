package sixweekpractice;
/*
Найдем факториал числа n рекурсивно.
 */

import java.util.Scanner;

public class Task01 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

//        int res = 1;
//        for (int i = 0; i <= n ; i++) {
//            res *= i;
//        }

//        int res = factorial(n);
        int res = factorialTail(n, 1);

        System.out.println("Factorial: " + res);
    }

    public static int factorial(int n) {
        if (n <= 1) {
            return 1;
        }
        return n* factorial(n - 1);
    }

    public static int factorialTail(int n, int result) {
        if (n <= 1) {
            return 1;
        }
        else
            return factorialTail(n - 1, n * result);
    }
}
